package m.corp.mysqltodos.repositories;

import m.corp.mysqltodos.models.Todo;
import m.corp.mysqltodos.models.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TodosRepository extends CrudRepository<Todo, Long> {
    Page<Todo> findByUserId(int userId, Pageable pageable);
}
