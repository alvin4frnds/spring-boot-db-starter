package m.corp.mysqltodos;

import m.corp.mysqltodos.repositories.TodosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import javax.sql.DataSource;

@SpringBootApplication
@EnableJpaRepositories("m.corp.mysqltodos.repositories")
@EnableJpaAuditing
@EntityScan("m.corp.mysqltodos.models")
public class MysqlTodosApplication {

    @Autowired
    DataSource dataSource;

    @Autowired
    TodosRepository todosRepository;


	public static void main(String[] args) {
		SpringApplication.run(MysqlTodosApplication.class, args);
	}
}
