package m.corp.mysqltodos.controllers;

import m.corp.mysqltodos.models.Todo;
import m.corp.mysqltodos.repositories.TodosRepository;
import m.corp.mysqltodos.repositories.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.swing.text.html.Option;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/todos")
public class TodosController {

    @Autowired
    TodosRepository todosRepository;
    @Autowired
    UsersRepository usersRepository;

    @GetMapping(value = "{userId}")
    public List<Todo> index(@PathVariable(value = "userId") int userId, Pageable pageable) {
        List<Todo> todos = new ArrayList<>();

        todosRepository.findByUserId(userId, pageable).forEach(todos::add);

        return todos;
    }

    @RequestMapping(value = "{userId}", method = RequestMethod.POST)
    public List<Todo> create(@PathVariable(value = "userId") int userId, @RequestBody Todo todo, Pageable pageable) {

        usersRepository.findById(userId).map(user -> {
            todo.setUser(user);
            return todosRepository.save(todo);
        });

        return this.index(userId, pageable);
    }

    @RequestMapping(method = RequestMethod.PATCH)
    public Todo update(@RequestBody Todo todo, Pageable pageable) {

        Optional<Todo> foundTodo = todosRepository.findById(todo.getId());

        if (! foundTodo.isPresent()) {
            todo.setId(null);

            this.create(todo.getUser().getId(), todo, pageable);
            return todo;
        }

        todosRepository.save(todo);
        return todo;
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public Todo delete(@PathVariable Long id) {
        Optional<Todo> foundTodo = todosRepository.findById(id);

        if (! foundTodo.isPresent()) return new Todo();

        todosRepository.delete(foundTodo.get());
        return foundTodo.get();
    }
}
