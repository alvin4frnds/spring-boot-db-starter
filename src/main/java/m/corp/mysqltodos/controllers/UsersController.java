package m.corp.mysqltodos.controllers;

import m.corp.mysqltodos.models.User;
import m.corp.mysqltodos.repositories.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/users")
public class UsersController {

    @Autowired
    UsersRepository usersRepository;

    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public User me(@PathVariable int id) {

        Optional<User> user = usersRepository.findById(id);
        return user.orElseGet(User::new);
    }

    @RequestMapping(method = RequestMethod.POST)
    public User create(@RequestBody User user) {

        return this.usersRepository.save(user);
    }
}
